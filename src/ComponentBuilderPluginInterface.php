<?php

namespace Drupal\component_builder;

use Drupal\Component\Plugin\DependentPluginInterface;

/**
 * Provides an interface for a navigation component plugin.
 */
interface ComponentBuilderPluginInterface extends DependentPluginInterface {

  /**
   * Returns the component label that is displayed in backend.
   */
  public function label(): string;

  /**
   * Returns the template name this component is using.
   */
  public function getTemplateName(): string;

  /**
   * Prepare the necessary variables for template_preprocess_node().
   */
  public function prepareVariables(array &$variables): void;

}
