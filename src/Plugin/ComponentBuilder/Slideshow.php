<?php

namespace Drupal\component_builder\Plugin\ComponentBuilder;

use Drupal\component_builder\Annotation\ComponentBuilder;
use Drupal\component_builder\ComponentBuilderBase;

/**
 * Provides 'Slideshow' component.
 *
 * @ComponentBuilder(
 *   id = "slideshow",
 *   label = @Translation("Slideshow"),
 *   group = @Translation("General components"),
 *   template = "slideshow"
 * )
 */
class Slideshow extends ComponentBuilderBase {

}
