<?php

namespace Drupal\component_builder\Plugin\ComponentBuilder;

use Drupal\component_builder\Annotation\ComponentBuilder;
use Drupal\component_builder\ComponentBuilderBase;
use Drupal\component_builder\Entity\ComponentItem;

/**
 * Provides 'Basic component' component.
 *
 * @ComponentBuilder(
 *   id = "basic_component",
 *   label = @Translation("Basic component"),
 *   group = @Translation("General components"),
 *   template = "basic_component"
 * )
 */
class BasicComponent extends ComponentBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function prepareVariables(array &$variables): void {
    if (isset($variables['elements']['#component_item'])) {
      $component_item = $variables['elements']['#component_item'];
      if ($component_item instanceof ComponentItem) {
        $field_link = $component_item->get('field_link')->getValue();
        if ($field_link) {
          if (!$field_link[0]['title']) {
            if ($variables['content']['title']['0']) {
              $variables['content']['field_link']['0']['#title'] = $variables['content']['title'][0]['#context']['value'];
            }
          }
        }
      }
    }
  }

}
