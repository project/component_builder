<?php

namespace Drupal\component_builder\Plugin\ComponentBuilder;

use Drupal\component_builder\Annotation\ComponentBuilder;
use Drupal\component_builder\ComponentBuilderBase;

/**
 * Provides 'Table' component.
 *
 * @ComponentBuilder(
 *   id = "table",
 *   label = @Translation("Table"),
 *   group = @Translation("General components"),
 *   template = "table"
 * )
 */
class Table extends ComponentBuilderBase {

}
