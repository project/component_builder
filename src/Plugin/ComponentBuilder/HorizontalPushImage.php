<?php

namespace Drupal\component_builder\Plugin\ComponentBuilder;

use Drupal\component_builder\Annotation\ComponentBuilder;
use Drupal\component_builder\ComponentBuilderBase;
use Drupal\component_builder\Entity\ComponentItem;

/**
 * Provides 'Horizontal push image' component.
 *
 * @ComponentBuilder(
 *   id = "horizontal_push_image",
 *   label = @Translation("Horizontal push image"),
 *   group = @Translation("General components"),
 *   template = "horizontal_push_image"
 * )
 */
class HorizontalPushImage extends ComponentBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function prepareVariables(array &$variables): void {
    if (isset($variables['elements']['#component_item'])) {
      $component_item = $variables['elements']['#component_item'];
      if ($component_item instanceof ComponentItem) {
        $field_link = $component_item->get('field_link')->getValue();
        if ($field_link) {
          if (!$field_link[0]['title']) {
            if ($variables['content']['title']['0']) {
              $variables['content']['field_link']['0']['#title'] = $variables['content']['title'][0]['#context']['value'];
            }
          }
        }
      }
    }
  }

}
