<?php

namespace Drupal\component_builder\Plugin\ComponentBuilder;

use Drupal\component_builder\Annotation\ComponentBuilder;
use Drupal\component_builder\ComponentBuilderBase;

/**
 * Provides 'Accordion' component.
 *
 * @ComponentBuilder(
 *   id = "accordion",
 *   label = @Translation("Accordion"),
 *   group = @Translation("General components"),
 *   template = "accordion"
 * )
 */
class Accordion extends ComponentBuilderBase {

}
