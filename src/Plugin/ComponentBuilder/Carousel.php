<?php

namespace Drupal\component_builder\Plugin\ComponentBuilder;

use Drupal\component_builder\Annotation\ComponentBuilder;
use Drupal\component_builder\ComponentBuilderBase;
use Drupal\component_builder\Entity\ComponentWrapper;

/**
 * Provides 'Carousel' component.
 *
 * @ComponentBuilder(
 *   id = "carousel",
 *   label = @Translation("Carousel"),
 *   group = @Translation("General components"),
 *   template = "carousel"
 * )
 */
class Carousel extends ComponentBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function prepareVariables(array &$variables): void {
    if (isset($variables['elements']['#component_wrapper'])) {
      $component_wrapper = $variables['elements']['#component_wrapper'];
      if ($component_wrapper instanceof ComponentWrapper) {
        $number = 1;
        $styles = $component_wrapper->get('field_styles')->value;
        if ($styles) {
          $styles = json_decode($styles, TRUE);
          if (isset($styles["carousel-columns"])) {
            $number = (int) $styles["carousel-columns"];
          }
        }
        $variables['attributes']['data-carousel-columns'][] = $number;
      }
    }
  }

}
