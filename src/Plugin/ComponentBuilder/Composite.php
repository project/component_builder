<?php

namespace Drupal\component_builder\Plugin\ComponentBuilder;

use Drupal\component_builder\Annotation\ComponentBuilder;
use Drupal\component_builder\ComponentBuilderBase;
use Drupal\component_builder\Entity\ComponentWrapper;

/**
 * Provides 'Composite' component.
 *
 * @ComponentBuilder(
 *   id = "composite",
 *   label = @Translation("Composite"),
 *   group = @Translation("General components"),
 *   template = "composite"
 * )
 */
class Composite extends ComponentBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function prepareVariables(array &$variables): void {
    if (isset($variables['elements']['#component_wrapper'])) {
      $component_wrapper = $variables['elements']['#component_wrapper'];
      if ($component_wrapper instanceof ComponentWrapper) {
        $number = 2;
        $class_options = '6_6';
        $styles = $component_wrapper->get('field_styles')->value;
        $options = $component_wrapper->get('field_options')->value;
        if ($styles) {
          $styles = json_decode($styles, TRUE);
          if (isset($styles["layout"])) {
            $class_options = str_replace('column_', '', $styles["layout"]);
          }
        }
        if ($options) {
          $options = json_decode($options, TRUE);
          if (isset($options["column"])) {
            $number = (int) $options["column"];
          }
        }
        $variables['attributes']['data-number-columns'][] = $number;
        $variables['attributes']['data-column-class'][] = $class_options;
      }
    }
  }

}
