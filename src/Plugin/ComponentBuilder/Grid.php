<?php

namespace Drupal\component_builder\Plugin\ComponentBuilder;

use Drupal\component_builder\Annotation\ComponentBuilder;
use Drupal\component_builder\ComponentBuilderBase;
use Drupal\component_builder\Entity\ComponentItem;
use Drupal\component_builder\Entity\ComponentWrapper;

/**
 * Provides 'Grid' component.
 *
 * @ComponentBuilder(
 *   id = "grid",
 *   label = @Translation("Grid"),
 *   group = @Translation("General components"),
 *   template = "grid"
 * )
 */
class Grid extends ComponentBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function prepareVariables(array &$variables): void {
    if (isset($variables['elements']['#component_wrapper'])) {
      $component_wrapper = $variables['elements']['#component_wrapper'];
      if ($component_wrapper instanceof ComponentWrapper) {
        $number = 1;
        $styles = $component_wrapper->get('field_styles')->value;
        if ($styles) {
          $styles = json_decode($styles, TRUE);
          if (isset($styles["number-columns"])) {
            $number = (int) $styles["number-columns"];
          }
        }
        $items = $component_wrapper->get('field_grid')->getValue();
        $number_items = count($items);
        if ($number_items && count($items) > $number) {
          for ($i = $number; $i < $number_items; $i++) {
            if (isset($variables['elements']['field_grid'][$i])) {
              unset($variables['content']['field_grid'][$i]);
            }
          }
        }
      }
    }

    if (isset($variables['elements']['#component_item'])) {
      $component_item = $variables['elements']['#component_item'];
      if ($component_item instanceof ComponentItem) {
        $field_link = $component_item->get('field_link')->getValue();
        if ($field_link) {
          if (!$field_link[0]['title']) {
            if ($variables['content']['title']['0']) {
              $variables['content']['field_link']['0']['#title'] = $variables['content']['title'][0]['#context']['value'];
            }
          }
        }
      }
    }
  }

}
