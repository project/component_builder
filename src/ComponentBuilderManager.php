<?php

namespace Drupal\component_builder;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Traversable;

/**
 * Plugin manager for navigation components.
 */
class ComponentBuilderManager extends DefaultPluginManager {

  /**
   * Constructs a new ComponentBuilderManager object.
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cacheBackend, ModuleHandlerInterface $moduleHandler) {
    parent::__construct(
      'Plugin/ComponentBuilder',
      $namespaces,
      $moduleHandler,
      'Drupal\component_builder\ComponentBuilderPluginInterface',
      'Drupal\component_builder\Annotation\ComponentBuilder',
    );
    $this->alterInfo('component_builder_info');
    $this->setCacheBackend($cacheBackend, 'component_builder_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $pluginId): void {
    parent::processDefinition($definition, $pluginId);
    $this->processDefinitionGroup($definition);
  }

  /**
   * Ensure that every component belong to a group.
   */
  protected function processDefinitionGroup(&$definition): void {
    if (empty($definition['group'])) {
      // Default to the human readable module name if the provider is a module;
      // otherwise the provider machine name is used.
      $definition['group'] = $this->getProviderName($definition['provider']);
    }
  }

  /**
   * Gets an instance of component plugin using template name.
   */
  public function getInstanceByTemplateName(string $template = ''): ?ComponentBuilderPluginInterface {
    $definitions = $this->getDefinitions();

    foreach ($definitions as $plugin_id => $definition) {
      if ($definition['template'] === $template) {
        return $this->createInstance($plugin_id);
      }
    }
    return NULL;
  }

}
