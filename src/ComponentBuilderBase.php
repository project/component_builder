<?php

namespace Drupal\component_builder;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;

/**
 * Provides a base class for navigation component plugins.
 */
abstract class ComponentBuilderBase extends PluginBase implements ComponentBuilderPluginInterface, ConfigurableInterface {

  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected FieldDefinitionInterface $fieldDefinition;

  /**
   * The formatter settings.
   *
   * @var array
   */
  protected array $settings;

  /**
   * The label display setting.
   *
   * @var string
   */
  protected string $label;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    $definition = $this->getPluginDefinition();
    return (string) $definition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateName(): string {
    $definition = $this->getPluginDefinition();
    return (string) $definition['template'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDirectory(): string {
    $definition = $this->getPluginDefinition();
    return (string) $definition['directory'];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareVariables(array &$variables): void {
    //    /** @var \Drupal\node\NodeInterface $node */
    //    $node = $variables['node'];
    //    $displayTitle = $node->get('field_display_title')->getString();
    //    $displayOption = $node->get('field_display_option')->referencedEntities();
    //    $displayOption = reset($displayOption);
    //    $templateName = $displayOption ? $displayOption->get('field_template_name')
    //      ->getString() : '';
    //
    //    $variables['display_title'] = $displayTitle;
    //    $variables['title'] = $node->label();
    //    if (!empty($templateName)) {
    //      $variables['section_class'] = 'navigation-section-' . $templateName;
    //    }
    //    $variables['#attached']['library'][] = 'petronas_navigation/petronas_aos';
    //
    //    $variables['navigation_items'] = [];
    //    foreach ($node->get('field_items')->referencedEntities() as $item) {
    //      $variables['navigation_items'][] = $item;
    //    }
  }

}
