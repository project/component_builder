<?php

namespace Drupal\component_builder\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\component_builder\Entity\ComponentWrapperInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\inline_entity_form\Form\EntityInlineForm;
use Drupal\node\NodeForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the inline form for order items.
 */
class ComponentWrapperInlineForm extends EntityInlineForm {

  /**
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected ExtensionPathResolver $extensionPathResolver;

  /**
   * Constructs the inline entity form controller.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, EntityTypeInterface $entity_type, ExtensionPathResolver $extensionPathResolver) {
    parent::__construct($entity_field_manager, $entity_type_manager, $module_handler, $entity_type);
    $this->extensionPathResolver = $extensionPathResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $entity_type,
      $container->get('extension.path.resolver'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function entityForm(array $entity_form, FormStateInterface $form_state) {
    $entity_form = parent::entityForm($entity_form, $form_state);
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof NodeForm) {
      $bundle = $form_object->getEntity()->bundle();
      $detect_field = $this->detectEntity();
      if (isset($detect_field[$bundle])) {
        $form_state->set('field_name_alter', $detect_field[$bundle]);
        $form_state->set('template_options', $entity_form['component_type']['widget']['#options']);
        $entity_form['component_attributes'] = [
          '#type' => 'container',
          '#attributes' => ['id' => 'component-attributes-wrapper'],
          '#weight' => $entity_form['component_type']['#weight'],
        ];
        $entity_form['config'] = [
          '#type' => 'details',
          '#title' => t('Config'),
          '#weight' => $entity_form['component_type']['#weight'],
          '#access' => FALSE,
        ];
        $entity_form['config']['field_region'] = $entity_form['field_region'];
        $entity_form['config']['field_weight'] = $entity_form['field_weight'];
        unset($entity_form['field_region']);
        unset($entity_form['field_weight']);
        $this->loadYmlComponents($entity_form, $form_state);
        $this->renderAttributeMarkup($entity_form, $form_state);
        $entity_form['#attached']['library'][] = 'component_builder/component_wrapper';
      }
    }

    return $entity_form;
  }

  /**
   * {@inheritdoc}
   */
  public function entityFormSubmit(array &$entity_form, FormStateInterface $form_state) {
    $values = $form_state->getValue($entity_form['#parents']);
    $component_id = $values['component_type'][0]['target_id'];
    $component_wrapper_templates = $form_state->get('component_wrapper_templates');
    if ($component_id) {
      $template_machine_name = $component_wrapper_templates[$component_id];
      $styles_json = '';
      $options_json = '';
      $display_json = '';
      if ($values['component_attributes'][$template_machine_name] && isset($values['component_attributes'][$template_machine_name]['styles'])) {
        $styles_json = json_encode($values['component_attributes'][$template_machine_name]['styles']);
      }
      if ($values['component_attributes'][$template_machine_name] && isset($values['component_attributes'][$template_machine_name]['options'])) {
        $options_json = json_encode($values['component_attributes'][$template_machine_name]['options']);
      }
      if ($values['component_attributes'][$template_machine_name] && isset($values['component_attributes'][$template_machine_name]['display_mode'])) {
        $display_json = json_encode($values['component_attributes'][$template_machine_name]['display_mode']);
      }
      $form_state->setValue(array_merge($entity_form['#parents'], ['field_styles']), [0 => ['value' => $styles_json]]);
      $form_state->setValue(array_merge($entity_form['#parents'], ['field_options']), [0 => ['value' => $options_json]]);
      $form_state->setValue(array_merge($entity_form['#parents'], ['field_display_mode']), [0 => ['value' => $display_json]]);
    }
    parent::entityFormSubmit($entity_form, $form_state);
  }

  public function renderAttributeMarkup(array &$entity_form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if (isset($trigger['#ief_row_delta'])) {
      $delta = $trigger['#ief_row_delta'];
      $component_wrapper = $form_state->get(
        [
          'inline_entity_form',
          $entity_form['#ief_id'],
          'entities',
          $delta,
          'entity',
        ]
      );
      if (isset($entity_form['component_attributes'])) {
        if ($component_wrapper instanceof ComponentWrapperInterface) {
          $component_id = $component_wrapper->get('component_type')
            ->getValue()[0]['target_id'];
          $component_wrapper_templates = $form_state->get('component_wrapper_templates');
          $template_machine_name = $component_wrapper_templates[$component_id];

          $styles = $component_wrapper->get('field_styles')->value;
          if ($styles) {
            $styles = json_decode($styles, TRUE);
            foreach ($styles as $key_style => $value_style) {
              $entity_form['component_attributes'][$template_machine_name]['styles'][$key_style]['#default_value'] = $value_style;
            }
          }
          $options = $component_wrapper->get('field_options')->value;
          if ($options) {
            $options = json_decode($options, TRUE);
            foreach ($options as $key_option => $value_option) {
              $entity_form['component_attributes'][$template_machine_name]['options'][$key_option]['#default_value'] = $value_option;
            }
          }
          $display = $component_wrapper->get('field_display_mode')->value;
          if ($display) {
            $display = json_decode($display, TRUE);
            $entity_form['component_attributes'][$template_machine_name]['display_mode']['#default_value'] = $display;
          }
        }
      }
    }
  }

  public function renderComponentAttributes(array &$entity_form, FormStateInterface $form_state) {
    $element = inline_entity_form_get_element($entity_form, $form_state);
    $ief_id = $element['#ief_id'];
    $entities = $form_state->get(['inline_entity_form', $ief_id, 'entities']);
    dump($form_state->get(['inline_entity_form', $ief_id, 'entities']));
    exit;
  }

  public function loadYmlComponents(array &$entity_form, FormStateInterface $form_state) {
    $field_name_alter = $form_state->get('field_name_alter');
    if (is_array($field_name_alter)) {
      $templates = [];
      foreach ($field_name_alter as $field_name) {
        if (in_array($field_name, $entity_form['#parents'])) {
          $options = $form_state->get('template_options');
          unset($options['_none']);
          $component_builder_path = $this->extensionPathResolver->getPath('module', 'component_builder');
          foreach ($options as $template_id => $component_type) {
            $component_type_name = strtolower($component_type);
            $component_type_machine = preg_replace('@[^a-z0-9-]+@', '_', $component_type_name);
            $templates[$template_id] = $component_type_machine;
            $component_yml_path = get_component_name($component_type_machine) . '.yml';
            $yml_path = $component_builder_path . '/components/component_' . $component_type_machine . '/' . $component_yml_path;
            $yml_content = file_get_contents($yml_path);
            $yml_content = Yaml::decode($yml_content);

            if ($yml_content) {
              $entity_form['component_attributes'][$component_type_machine] = [
                '#type' => 'details',
                '#attributes' => [
                  'id' => 'component-attributes-wrapper__' . $component_type_machine,
                  'class' => [
                    'component-attributes-wrapper__item',
                    'component-attributes-wrapper__' . $component_type_machine,
                  ],
                ],
                '#title' => t('Properties'),
              ];
              foreach ($yml_content as $key => $values) {
                if (!in_array($key, ['name', 'machine_name'])) {
                  if (is_array(reset($values))) {
                    $entity_form['component_attributes'][$component_type_machine][$key] = [
                      '#type' => 'details',
                      '#attributes' => [
                        'id' => 'component-attributes-wrapper__' . $component_type_machine . '--' . $key,
                        'class' => ['component-attributes-wrapper__item--' . $key],
                      ],
                      '#title' => $key,
                    ];
                    foreach ($values as $option_key => $option_value) {
                      $options = ['_none' => '-- None --'];
                      $options += $option_value;
                      $entity_form['component_attributes'][$component_type_machine][$key][$option_key] = [
                        '#type' => 'select',
                        '#title' => 'Select ' . $option_key,
                        '#options' => $options,
                      ];
                    }
                  }
                  else {
                    $options = ['_none' => '-- None --'];
                    $options += $values;
                    $entity_form['component_attributes'][$component_type_machine][$key] = [
                      '#type' => 'select',
                      '#title' => 'Select ' . $key,
                      '#options' => $options,
                    ];
                  }
                }
              }
            }
          }
        }
      }
      $form_state->set('component_wrapper_templates', $templates);
    }
  }

  public function detectEntity(): array {
    static $drupal_static_fast_detect_entity_id;
    if (!isset($drupal_static_fast_detect_entity_id)) {
      $storage = $this->entityTypeManager->getStorage('field_storage_config');
      $entity_ids = $storage->getQuery()
        ->accessCheck(TRUE)
        ->execute();
      $field_storages = $storage->loadMultipleOverrideFree($entity_ids);
      $drupal_static_fast_detect_entity_id = [];
      foreach ($field_storages as $field_storage) {
        $field_storage_name = $field_storage->getName();
        $filed_storage_type = $field_storage->getType();
        $allowed_type = [
          'entity_reference',
        ];
        if (in_array($filed_storage_type, $allowed_type, TRUE)) {
          $filed_storage_target_type = $field_storage->getSettings()['target_type'];
          if ($filed_storage_target_type === 'component_wrapper') {
            foreach ($field_storage->getBundles() as $bundle) {
              $drupal_static_fast_detect_entity_id[$bundle][] = $field_storage_name;
            }
          }
        }
      }
    }
    return $drupal_static_fast_detect_entity_id;
  }

}
