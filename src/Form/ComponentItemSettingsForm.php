<?php

namespace Drupal\component_builder\Form;

use Drupal;
use Drupal\component_builder\ImportConfigComponent;
use Drupal\component_wrapper\Entity\ComponentWrapper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ComponentItemSettingsForm
 *
 * @package Drupal\component_builder\Form
 */
class ComponentItemSettingsForm extends ConfigFormBase {

  const SETTING = 'component_item.component_item_setting';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $file_system;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected ExtensionPathResolver $extensionPathResolver;

  /**
   * The Import config component services.
   *
   * @var \Drupal\component_builder\ImportConfigComponent
   */
  protected ImportConfigComponent $importConfigComponent;

  /**
   * Constructs a new ComponentItemSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system object.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   * @param \Drupal\component_builder\ImportConfigComponent $import_config_component
   *   The extension path resolver.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystemInterface $file_system, ExtensionPathResolver $extensionPathResolver, ImportConfigComponent $import_config_component) {
    parent::__construct($config_factory);
    $this->file_system = $file_system;
    $this->extensionPathResolver = $extensionPathResolver;
    $this->importConfigComponent = $import_config_component;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('extension.path.resolver'),
      $container->get('component_builder.import_config_component')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'component_item_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $table = $values['table'];
    $definitions = $form_state->get('definitions');
    foreach ($table as $rows) {
      foreach ($rows as $componentId => $value) {
        if ($componentId == 'table' && $value == 1) {
          array_unshift($table, ['table_column' => 1]);
        }
      }
    }
    $config = $this->config(self::SETTING);
    foreach ($table as $rows) {
      foreach ($rows as $componentId => $value) {
        if (!$config->get($componentId) && $value == 1 && !in_array($componentId, ['table_column'])) {
          if (isset($definitions[$componentId])) {
            $id = $definitions[$componentId]['id'];
            $label = $definitions[$componentId]['label']->__toString();
            $this->importConfigComponent->createComponentItem($id, $label);
          }
        }
        elseif ($componentId == 'table_column' && !$config->get($componentId) && $value == 1) {
          $id = 'table_column';
          $label = 'Table Column';
          $config->set('table_column', $config->get('table'));
          $this->importConfigComponent->createComponentItem($id, $label);
        }
        $config->set($componentId, $value);
      }
    }
    $config->save();
    Drupal::service('cache.render')->invalidateAll();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $pluginManager = Drupal::service('plugin.manager.component_builder');
    $definitions = $pluginManager->getDefinitions();
    $groups = [];
    foreach ($definitions as $config) {
      $groups[$config['group']->__toString()][] = $config;
      usort($groups[$config['group']->__toString()], function ($a, $b) {
        return strcmp($a['label']->__toString(), $b['label']->__toString());
      });
    }
    ksort($groups);
    $groupKeys = array_keys($groups);
    $rowsLength = count(max($groups));
    $header = array_keys($groups);
    $form_state->set('definitions', $definitions);
    $form['components'] = [
      '#type' => 'details',
      '#title' => t('Components'),
      '#open' => TRUE,
    ];
    $form['components']['table'] = [
      '#type' => 'table',
      '#tree' => TRUE,
      '#header' => $header,
      '#empty' => t('No Language available.'),
    ];
    for ($i = 0; $i < $rowsLength; $i++) {
      $row = [];
      foreach ($groupKeys as $key) {
        if (isset($groups[$key][$i])) {
          $config = $groups[$key][$i];
          $name = $config['label'];
          $id = $config['id'];
          $row[$id] = [
            '#type' => 'checkbox',
            '#title' => $name,
            '#default_value' => $this->config(self::SETTING)->get($id),
          ];
        }
      }
      $form['components']['table'][$i] = $row;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTING,
    ];
  }

}
