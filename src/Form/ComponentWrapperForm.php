<?php

namespace Drupal\component_builder\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the component_wrapper entity edit forms.
 *
 * @ingroup component_wrapper
 */
class ComponentWrapperForm extends ContentEntityForm {

  /**
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected ExtensionPathResolver $extensionPathResolver;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, ExtensionPathResolver $extensionPathResolver) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->extensionPathResolver = $extensionPathResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('extension.path.resolver'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'][] = 'component_builder/component_wrapper';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.component_wrapper.collection');
    $entity = $this->getEntity();
    $entity->save();
  }

  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $component_types = $form['component_type']['widget']['#options'];
    $this->loadYmlComponents($form, $component_types);
    return $form;
  }

  public function loadYmlComponents(array &$form, array $component_types) {
    unset($component_types['_none']);
    $component_builder_path = $this->extensionPathResolver->getPath('module', 'component_builder');
    foreach ($component_types as $component_type) {
      $component_type_machine = preg_replace('@[^a-z0-9-]+@', '_', strtolower($component_type));
      $yml_path = $component_builder_path . '/components/component_' . $component_type_machine . '/component.yml';
      $yml_content = file_get_contents($yml_path);
      if ($yml_content) {
        $yml_content = Yaml::decode($yml_content);
        foreach ($yml_content as $key => $value) {
          if (!in_array($key, ['name', 'machine_name'])) {
            $options = ['_none' => '-- Select ' . $key . ' --'];
            $options += $value;
            $form[$key] = [
              '#type' => 'select',
              '#options' => $options,
            ];
          }
        }
      }
    }
  }

}
