<?php

namespace Drupal\component_builder\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ComponentItemForm
 *
 * @package Drupal\component_builder\Form
 */
class ComponentItemForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->setNewRevision();
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['revision_information']['#access'] = FALSE;

    if (isset($form['revision_log_message'])) {
      unset($form['revision_log_message']);
    }

    if (isset($form['field_task']['widget']['actions'])) {
      if ($form['field_task']['widget']['actions']['ief_add']) {
        $form['field_task']['widget']['actions']['ief_add']['#value'] = t('Add new component item');
      }
      if ($form['field_task']['widget']['actions']['ief_add_existing']) {
        $form['field_task']['widget']['actions']['ief_add_existing']['#value'] = t('Add existing component item');
      }
    }
    return $form;
  }

}
