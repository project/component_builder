<?php

namespace Drupal\component_builder;

use Drupal;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\PhpStorage\PhpStorageFactory;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\Yaml\Yaml;

/**
 * The ImportConfigComponent service.
 */
class ImportConfigComponent {

  const COMPONENT_EXCEPT = ['table_column'];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ImportConfigComponent constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager = NULL, EntityTypeManagerInterface $entity_type_manager = NULL, ModuleHandlerInterface $module_handler = NULL) {
    $this->entityFieldManager = Drupal::service('entity_field.manager');
    $this->moduleHandler = $module_handler ?: Drupal::moduleHandler();
    $this->entityTypeManager = $entity_type_manager ?: Drupal::entityTypeManager();
  }

  /**
   * {@inheritdoc}
   */
  public function createComponentItem(string $bundle, string $label) {
    $componentItemTypeStorage = $this->entityTypeManager->getStorage('component_item_type');
    if ($componentItemTypeStorage) {
      $componentType = $this->entityTypeManager->getStorage('component_item_type')
        ->load($bundle);
      if (!$componentType) {
        if ($bundle === 'composite') {
          $this->createTerm($label);
          return TRUE;
        }
        else {
          $componentName = get_component_name($bundle);
          $componentPath = $this->getPathComponentItem($componentName);
          $pathConfig = $componentPath . "/config/component_item.component_item_type.$bundle.yml";
          $values = $this->parseYaml($pathConfig);
          if ($values) {
            $newComponentType = $componentItemTypeStorage->create($values);
            $newComponentType->save();
            $userRoleStorage = $this->entityTypeManager->getStorage('user_role');
            $role = $userRoleStorage->load('administrator');
            $issueAccessPermissions = [
              "create $bundle content",
              "delete any $bundle content",
              "delete own $bundle content",
              "edit any $bundle content",
              "edit own $bundle content",
            ];
            foreach ($issueAccessPermissions as $permission) {
              $role->grantPermission($permission);
            }
            $role->save();
            // $this->generatePathautoPattern('node', $machine_name);
            $this->createFieldsComponent('component_item', $bundle);
            $this->createDisplay('component_item', $bundle);
            if (!in_array($bundle, self::COMPONENT_EXCEPT)) {
              $this->createTerm($label);
            }
            return $bundle;
          }
        }
      }
      else {
        return NULL;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createFieldsComponent($entity = '', $bundle = ''): void {
    $componentName = get_component_name($bundle);
    $componentPath = $this->getPathComponentItem($componentName);
    $configPath = $componentPath . '/config/config.yml';
    $yml = $this->parseYaml($configPath);
    if ($yml && isset($yml['fields'])) {
      $fieldDefines = $this->entityFieldManager->getFieldDefinitions($entity, $bundle);
      $fields = $yml['fields'];
      foreach ($fields as $fieldName => $label) {
        if (!array_key_exists($fieldName, $fieldDefines)) {
          $this->createField($fieldName, $entity, $bundle, $componentPath);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createDisplay($entity, $bundle): void {
    $componentName = get_component_name($bundle);
    $componentPath = $this->getPathComponentItem($componentName);
    $this->setFormDisplay($componentPath, $entity, $bundle, 'default');
    $this->setViewDisplay($componentPath, $entity, $bundle, 'default');
  }

  /**
   * {@inheritdoc}
   */
  public function createField($fieldName = '', $entity = '', $bundle = '', $componentPath = ''): void {
    $fieldStorage = FieldStorageConfig::loadByName($entity, $fieldName);
    if (!$fieldStorage) {
      $pathStorageConfig = $componentPath . "/config/field.storage.component_item.$fieldName.yml";
      $this->importFieldStorageYmlConfig($pathStorageConfig);
    }
    $pathConfig = $componentPath . "/config/field.field.component_item.$bundle.$fieldName.yml";
    $this->importFieldYmlConfig($pathConfig);
  }

  /**
   * {@inheritdoc}
   */
  public function setFormDisplay($componentPath, $entityType, $bundle, $form_mode) {
    $entityFormDisplayStorage = $this->entityTypeManager->getStorage('entity_form_display');
    $entityFormDisplay = $entityFormDisplayStorage->load("$entityType.$bundle.$form_mode");
    if (!$entityFormDisplay) {
      $pathConfig = $componentPath . "/config/core.entity_form_display.$entityType.$bundle.$form_mode.yml";
      $values = $this->parseYaml($pathConfig);
      if ($values) {
        $entity_form_display = $entityFormDisplayStorage->create($values);
        $entity_form_display->save();
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setViewDisplay($componentPath, $entityType, $bundle, $form_mode) {
    $entityFormDisplayStorage = $this->entityTypeManager->getStorage('entity_view_display');
    $entityFormDisplay = $entityFormDisplayStorage->load("$entityType.$bundle.$form_mode");
    if (!$entityFormDisplay) {
      $pathConfig = $componentPath . "/config/core.entity_view_display.$entityType.$bundle.$form_mode.yml";
      $values = $this->parseYaml($pathConfig);
      if ($values) {
        $entity_form_display = $entityFormDisplayStorage->create($values);
        $entity_form_display->save();
      }
    }
    return TRUE;
  }

  /**
   * Flush all cache.
   */
  public function flushAll() {
    drupal_flush_all_caches();
  }

  /**
   * Clear cache bin.
   *
   * @param string $cache_name
   *   Cache name.
   */
  public function flush($cache_name = '') {
    if (empty($cache_name)) {
      $this->flushAll();
    }
    else {
      foreach (Cache::getBins() as $service_id => $cache_backend) {
        if (is_array($cache_name)) {
          if (in_array($service_id, $cache_name)) {
            $cache_backend->deleteAll();
          }
        }
        else {
          if ($cache_name == 'twig') {
            PhpStorageFactory::get('twig')->deleteAll();
          }
          elseif ($service_id == $cache_name) {
            $cache_backend->deleteAll();
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function importFieldYmlConfig($path_to_file): void {
    $field_string = file_get_contents($path_to_file);
    $field_config = Yaml::parse($field_string);
    if ($field_config) {
      $fieldConfigStorage = $this->entityTypeManager->getStorage('field_config');
      $fieldStorage = $fieldConfigStorage->create($field_config);
      $fieldStorage->save();
      $this->flush('twig');
    }
  }

  public function createTerm(string $label) {
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')
      ->load('component_types');
    if (!$vocabulary) {
      $this->createVocabulary('Component Types');
    }
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()
      ->condition('name', $label)
      ->condition('vid', 'component_types')
      ->execute();
    if (!$term) {
      $term = Term::create([
        'name' => $label,
        'description' => '',
        'parent' => [0],
        'vid' => 'component_types',
      ]);
      $term->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createVocabulary(string $label) {
    $machine = preg_replace('@[^a-z0-9-]+@', '_', strtolower($label));
    $vocabulary = Vocabulary::create([
      'name' => $label,
      'description' => $label,
      'vid' => $machine,
    ]);
    $vocabulary->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getPathComponentItem(string $componentName): string {
    $module_path = $this->moduleHandler->getModule('component_builder')
      ->getPath();
    return $module_path . '/components/' . $componentName;
  }

  /**
   * {@inheritdoc}
   */
  public function parseYaml($path): array {
    return Yaml::parse(file_get_contents($path)) ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function importFieldStorageYmlConfig($path) {
    $fieldConfig = $this->parseYaml($path);
    if (!empty($fieldConfig)) {
      $storage = $this->entityTypeManager->getStorage('field_storage_config');
      $fieldStorage = $storage->create($fieldConfig);
      $fieldStorage->save();
      $this->flush('twig');
    }
    return $fieldStorage;
  }

}
