<?php

namespace Drupal\component_builder\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a ComponentBuilder annotation object.
 *
 * @Annotation
 */
class ComponentBuilder extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the component.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The group in which the component belong to.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $group;

  /**
   * The template name that is used to render the component.
   *
   * @var string
   */
  public string $template = '';

}
