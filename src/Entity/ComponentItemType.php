<?php

namespace Drupal\component_builder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Component Item type entity class.
 *
 * @ConfigEntityType(
 *   id = "component_item_type",
 *   label = @Translation("Component Item type"),
 *   label_collection = @Translation("Component Item types"),
 *   label_singular = @Translation("Component Item type"),
 *   label_plural = @Translation("Component Item types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Component Item type",
 *     plural = "@count Component Item types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" =
 *   "Drupal\component_builder\ComponentItemTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\component_builder\Form\ComponentItemTypeForm",
 *       "edit" = "Drupal\component_builder\Form\ComponentItemTypeForm",
 *       "duplicate" = "Drupal\component_builder\Form\ComponentItemTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "component_item_type",
 *   admin_permission = "administer component_item_type",
 *   bundle_of = "component_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/component-item/types/add",
 *     "edit-form" =
 *   "/admin/structure/component-item/types/{component_item_type}/edit",
 *     "duplicate-form" =
 *   "/admin/structure/component-item/types/{component_item_type}/duplicate",
 *     "delete-form" =
 *   "/admin/structure/component-item/types/{component_item_type}/delete",
 *     "collection" = "/admin/structure/component-item/types"
 *   }
 * )
 */
class ComponentItemType extends ConfigEntityBundleBase implements ComponentItemTypeInterface {

  /**
   * The component item type description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
