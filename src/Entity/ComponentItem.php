<?php

namespace Drupal\component_builder\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Component Item class.
 *
 * @ContentEntityType(
 *   id = "component_item",
 *   label = @Translation("Item"),
 *   label_collection = @Translation("Items"),
 *   label_singular = @Translation("Item"),
 *   label_plural = @Translation("Items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Item",
 *     plural = "@count Items",
 *   ),
 *   bundle_label = @Translation("Component Item type"),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\component_builder\ComponentItemListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "moderation" = "Drupal\content_moderation\Entity\Handler",
 *     "form" = {
 *       "default" = "Drupal\component_builder\Form\ComponentItemForm",
 *       "add" = "Drupal\component_builder\Form\ComponentItemForm",
 *       "edit" = "Drupal\component_builder\Form\ComponentItemForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "inline_form" = "Drupal\component_builder\Form\ComponentItemInlineForm",
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" =
 *   "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer component_item",
 *   permission_granularity = "bundle",
 *   translatable = TRUE,
 *   base_table = "component_item",
 *   data_table = "component_item_field_data",
 *   revision_table = "component_item_revision",
 *   revision_data_table = "component_item_field_revision",
 *   show_revision_ui = TRUE,
 *   entity_keys = {
 *     "id" = "component_item_id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "cms_title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/component-item/{component_item}",
 *     "add-page" = "/admin/structure/component-item/add",
 *     "add-form" =
 *   "/admin/structure/component-item/add/{component_item_type}",
 *     "edit-form" = "/admin/structure/component-item/{component_item}/edit",
 *     "delete-form" =
 *   "/admin/structure/component-item/{component_item}/delete",
 *     "delete-multiple-form" = "/admin/structure/component-item/delete",
 *     "collection" = "/admin/structure/component-items",
 *   },
 *   bundle_entity_type = "component_item_type",
 *   field_ui_base_route = "entity.component_item_type.edit_form",
 * )
 */
class ComponentItem extends ContentEntityBase implements ComponentItemInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['cms_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('CMS title'))
      ->setDescription(t('The CMS title title.'))
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Content Title'))
      ->setDescription(t('The entity title.'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid']
      ->setLabel(t('Author'))
      ->setDescription(t('The entity author.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status']
      ->setLabel(t('Published'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The time when the entity was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The time when the entity was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

}
