<?php

namespace Drupal\component_builder\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Component Wrapper class.
 *
 * @ContentEntityType(
 *   id = "component_wrapper",
 *   label = @Translation("Component Wrapper"),
 *   label_collection = @Translation("Components"),
 *   label_singular = @Translation("Component"),
 *   label_plural = @Translation("Components"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Component Wrapper",
 *     plural = "@count Component Wrappers",
 *   ),
 *   bundle_label = @Translation("Component Wrapper type"),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "query_access" = "Drupal\entity\QueryAccess\QueryAccessHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\component_builder\ComponentWrapperListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "moderation" = "Drupal\content_moderation\Entity\Handler",
 *     "form" = {
 *       "default" = "Drupal\component_builder\Form\ComponentWrapperForm",
 *       "add" = "Drupal\component_builder\Form\ComponentWrapperForm",
 *       "edit" = "Drupal\component_builder\Form\ComponentWrapperForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "inline_form" =
 *   "Drupal\component_builder\Form\ComponentWrapperInlineForm",
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" =
 *   "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer component_wrapper",
 *   permission_granularity = "bundle",
 *   translatable = TRUE,
 *   base_table = "component_wrapper",
 *   data_table = "component_wrapper_field_data",
 *   revision_table = "component_wrapper_revision",
 *   revision_data_table = "component_wrapper_field_revision",
 *   show_revision_ui = TRUE,
 *   entity_keys = {
 *     "id" = "component_wrapper_id",
 *     "revision" = "vid",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/component-wrapper/{component_wrapper}",
 *     "add-form" = "/admin/structure/component-wrapper/add",
 *     "edit-form" =
 *   "/admin/structure/component-wrapper/{component_wrapper}/edit",
 *     "delete-form" =
 *   "/admin/structure/component-wrapper/{component_wrapper}/delete",
 *     "delete-multiple-form" = "/admin/structure/component-wrapper/delete",
 *     "collection" = "/admin/structure/component-wrappers",
 *   },
 *   field_ui_base_route = "entity.component_wrapper.settings",
 * )
 */
class ComponentWrapper extends ContentEntityBase implements ComponentWrapperInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateMachineName() {
    $component_type = $this->get('component_type')->referencedEntities();
    if (is_array($component_type)) {
      $component_type = reset($component_type);
      $template_name = $component_type ? $component_type->get('name')->value : '';
      if ($template_name) {
        return preg_replace('@[^a-z0-9-]+@', '_', strtolower($template_name));
      }
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('CMS Title'))
      ->setDescription(t('The entity title.'))
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['display_title'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Display title'))
      ->setTranslatable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['component_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Component Types'))
      ->setSetting('handler_settings', ['target_bundles' => ['taxonomy_term' => 'component_types']])
      ->setSetting('target_type', 'taxonomy_term')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_region'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Region'))
      ->setDescription(t('The region of component.'))
      ->setSetting('allowed_values', [
        'top_widget' => t('Top Widget'),
        'right_widget' => t('Right Widget'),
        'bottom_widget' => t('Bottom Widget'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('Weight.'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_header'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Header'))
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 4,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_accordion'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Accordion'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_item')
      ->setSetting('handler_settings', ['target_bundles' => ['accordion' => 'accordion']])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => t('accordion'),
          'label_plural' => t('accordions'),
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_grid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Grid'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_item')
      ->setSetting('handler_settings', ['target_bundles' => ['grid' => 'grid']])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => t('grid'),
          'label_plural' => t('grids'),
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_basic_component'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Basic component	'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_item')
      ->setSetting('handler_settings', ['target_bundles' => ['basic_component' => 'basic_component']])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => t('Basic component'),
          'label_plural' => t('Basic components'),
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_carousel'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Carousel'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_item')
      ->setSetting('handler_settings', ['target_bundles' => ['carousel' => 'carousel']])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => t('Carousel'),
          'label_plural' => t('Carousels'),
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_horizontal_push_image'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Horizontal push image'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_item')
      ->setSetting('handler_settings', ['target_bundles' => ['horizontal_push_image' => 'horizontal_push_image']])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => t('Horizontal push image'),
          'label_plural' => t('Horizontal push images'),
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_table'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Table'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_item')
      ->setSetting('handler_settings', ['target_bundles' => ['table' => 'table']])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
        'settings' => [
          'override_labels' => TRUE,
          'label_singular' => t('Table'),
          'label_plural' => t('Tables'),
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_left_column'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Left Column'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_wrapper')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_middle_column'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Middle Column'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_wrapper')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_right_column'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Right Column'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'component_wrapper')
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => [
          'view_mode' => 'full',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_footer'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Footer'))
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 9,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_styles'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Styles'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 25,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_options'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Options'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 25,
        'settings' => [
          'rows' => 4,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['field_display_mode'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Display mode'))
      ->setDescription(t('Display mode.'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']
      ->setLabel(t('Published'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['uid']
      ->setLabel(t('Author'))
      ->setDescription(t('The entity author.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The time when the entity was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setRevisionable(TRUE)
      ->setDescription(t('The time when the entity was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

}

