<?php

namespace Drupal\component_builder\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for product types.
 */
interface ComponentItemTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
