<?php

namespace Drupal\component_builder;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\entity\BulkFormEntityListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for component_wrapper entity.
 *
 * @ingroup component_wrapper
 */
class ComponentWrapperListBuilder extends BulkFormEntityListBuilder {

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager')->getStorage('action'),
      $container->get('form_builder'),
      $container->get('url_generator')
    );
  }

  /**
   * Constructs a new BibleBookListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $action_storage
   *   The action storage.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $entity_storage, EntityStorageInterface $action_storage, FormBuilderInterface $form_builder, UrlGeneratorInterface $url_generator) {
    parent::__construct($entity_type, $entity_storage, $action_storage, $form_builder);
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the Component Wrapper list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header = [
      'name' => [
        'data' => $this->t('Name'),
        'field' => 'title',
        'specifier' => 'title',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'template' => [
        'data' => $this->t('Template'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
      'created' => [
        'data' => $this->t('Created'),
        'field' => 'created',
        'specifier' => 'created',
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ],
    ];
    if (Drupal::languageManager()->isMultilingual()) {
      $header['language_name'] = [
        'data' => $this->t('Language'),
        'class' => [RESPONSIVE_PRIORITY_LOW],
      ];
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $uri = $entity->toUrl();
    $row['name']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $uri,
    ];
    $row['template']['data'] = [
      '#markup' => $entity->getTemplateMachineName(),
    ];
    $row['created']['data'] = [
      '#markup' => $entity->get('created')->value ? date('d/m/Y', $entity->get('created')->value) : '',
    ];
    $language_manager = Drupal::languageManager();
    if ($language_manager->isMultilingual()) {
      $row['language_name'] = [
        '#markup' => $entity->language()->getName(),
      ];
    }
    $row['operations']['data'] = $this->buildOperations($entity);

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->storage->getQuery();
    $entity_query->accessCheck(TRUE);
    $entity_query->condition('uid', 0, '<>');
    $entity_query->pager(50);

    $header = $this->buildHeader();
    $entity_query->tableSort($header);
    $components = $entity_query->execute();
    return $this->storage->loadMultiple($components);
  }

}
