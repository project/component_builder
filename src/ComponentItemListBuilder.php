<?php

namespace Drupal\component_builder;

use Drupal\commerce_product\Entity\ProductType;
use Drupal\component_builder\Entity\ComponentItemType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;
use Drupal\entity\BulkFormEntityListBuilder;

/**
 * Defines the list builder for products.
 */
class ComponentItemListBuilder extends BulkFormEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = t('Title');
    $header['type'] = t('Type');
    $header['status'] = t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\component_builder\Entity\ComponentItemTypeInterface $entity_type_with_bundle_type */
    $entity_type_with_bundle_type = ComponentItemType::load($entity->bundle());
    $row['title']['data'] = [
        '#type' => 'link',
        '#title' => $entity->label(),
      ] + $entity->toUrl()->toRenderArray();
    $row['type'] = [
      '#markup' => $entity_type_with_bundle_type->label(),
    ];
    $row['status'] = [
      '#markup' => $entity->isPublished() ? $this->t('published') : $this->t('not published'),
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    // custom operations here.

    return $operations;
  }

}
