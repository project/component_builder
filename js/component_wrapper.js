/**
 * @file
 * Behaviors for the component_wrapper widget in node add form.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.componentWrapper = Drupal.componentWrapper || {};
  Drupal.behaviors.componentWrapper = {
    attach: function (context) {
      Drupal.componentWrapper.setEvent(context);
    }
  }

  Drupal.componentWrapper.setEvent = function (context) {
    $('.field--type-entity-reference.field--name-component-type', context).once('name-component-type').each(function () {
      var $select = $(this).find('select');
      if ($select.length > 0) {
        Drupal.componentWrapper.showFieldReferenceWithComponentType($select);
        $select.change(function () {
          Drupal.componentWrapper.showFieldReferenceWithComponentType($(this))
        })
      }

      let $selectProperties = $('#component-attributes-wrapper__composite--options [id*=component-attributes-composite-options-column]');
      Drupal.componentWrapper.showFieldReferenceWithColumn($selectProperties);
      $('#component-attributes-wrapper__composite--options [id*=component-attributes-composite-options-column]', context).once('change-properties-component').each(function () {
        $(this).change(function () {
          Drupal.componentWrapper.showFieldReferenceWithColumn($(this));
        })
      })
    })
  }

  Drupal.componentWrapper.showFieldReferenceWithComponentType = function ($selector, allowedFields = []) {
    var $parentSelect = $selector.closest('.field--type-entity-reference.field--name-component-type');
    var $parentIEF = $parentSelect.parent();
    var $complexField = $parentIEF.children('.field--type-entity-reference.field--widget-inline-entity-form-complex, .field--type-entity-reference.field--widget-inline-entity-form-simple');
    Drupal.componentWrapper.displayElement($complexField, 'none');
    var textSelect = $selector.find('option:selected').text();
    console.log($parentSelect);
    console.log(textSelect);
    if (textSelect.length > 0) {
      textSelect = textSelect.toLowerCase().replaceAll(' ', '-');
      const machineComponent = textSelect.replaceAll('-', '_');
      if (allowedFields.length === 0) {
        var allowedFields = [];
        if (machineComponent == 'composite') {
          const $select = $parentIEF.find('#component-attributes-wrapper__composite--options [id*=component-attributes-composite-options-column]');
          if ($select.length > 0) {
            var val = $select.val();
            if (val != '_none') {
              allowedFields = Drupal.componentWrapper.getCompositeAllowed(val, false);
            }
          }
        } else {
          allowedFields = [textSelect];
        }
      }
      var allowItemClass = [];
      for (let key in allowedFields) {
        let fieldClass = 'field--name-field-' + allowedFields[key];
        let itemClass = '.field--type-entity-reference.field--widget-inline-entity-form-complex.' + fieldClass + ', .field--type-entity-reference.field--widget-inline-entity-form-simple.' + fieldClass;
        allowItemClass.push(itemClass);
      }
      if (allowItemClass.length > 0) {
        allowItemClass = allowItemClass.join(',');
        var $allowItem = $parentIEF.children(allowItemClass);
        if ($allowItem.length > 0) {
          Drupal.componentWrapper.displayElement($parentIEF.children(allowItemClass));
        }
      }

      Drupal.componentWrapper.displayElement($parentIEF.find('.component-attributes-wrapper__item'), 'none');
      var $properties = $parentIEF.find('#component-attributes-wrapper__' + textSelect.replaceAll('-', '_'));
      Drupal.componentWrapper.displayElement($properties);
    }
  }

  Drupal.componentWrapper.showFieldReferenceWithColumn = function ($selector) {
    $selector = $($selector);
    if ($selector.length != 1) {
      return;
    }
    var numberColumn = $selector.val();
    console.log(numberColumn)
    var $parentWrapperProperties = $selector.closest('#component-attributes-wrapper');
    var $parentIEF = $parentWrapperProperties.parent();

    let allowedColumnClass = [];
    let allColumnClass = [
      '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-left-column',
      '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-middle-column',
      '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-right-column',
      '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-left-column',
      '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-middle-column',
      '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-right-column'
    ];
    if (numberColumn == 1) {
      allowedColumnClass = [
        '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-left-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-left-column'
      ];
    } else if (numberColumn == 2) {
      allowedColumnClass = [
        '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-left-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-left-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-right-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-right-column'
      ];
    } else if (numberColumn == 3) {
      allowedColumnClass = allColumnClass;
    }
    let allowedColumnClassString = allowedColumnClass.join(',')
    let allColumnClassString = allColumnClass.join(',')
    Drupal.componentWrapper.displayElement($parentIEF.find(allColumnClassString), 'none');
    Drupal.componentWrapper.displayElement($parentIEF.find(allowedColumnClassString));
  }

  Drupal.componentWrapper.displayElement = function (element, display = 'block') {
    $(element).css({'display': display});
  }

  Drupal.componentWrapper.getCompositeAllowed = function (numberColumn, full_class = true) {
    var allowedColumnClass = [];
    if (full_class) {
      var allColumnClass = [
        '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-left-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-middle-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-right-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-left-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-middle-column',
        '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-right-column'
      ];
      if (numberColumn == 1) {
        allowedColumnClass = [
          '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-left-column',
          '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-left-column'
        ];
      } else if (numberColumn == 2) {
        allowedColumnClass = [
          '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-left-column',
          '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-left-column',
          '.field--type-entity-reference.field--widget-inline-entity-form-complex.field--name-field-right-column',
          '.field--type-entity-reference.field--widget-inline-entity-form-simple.field--name-field-right-column'
        ];
      } else if (numberColumn == 3) {
        allowedColumnClass = allColumnClass;
      }
    } else {
      var allColumnClass = [
        'left-column',
        'middle-column',
        'right-column',
      ];
      if (numberColumn == 1) {
        allowedColumnClass = [
          'left-column',
        ];
      } else if (numberColumn == 2) {
        allowedColumnClass = [
          'left-column',
          'right-column',
        ];
      } else if (numberColumn == 3) {
        allowedColumnClass = allColumnClass;
      }
    }
    return allowedColumnClass
  }

})(jQuery, Drupal);